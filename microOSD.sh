#microOS Desktop "comfy slippers" script

#System Layering and Tweaks

#get prepared for landing - my personal file folders that I like to use
#-p says to make the directory if the parent folder doesn't exist as a saftey measure
mkdir -p ~/Downloads/Software
mkdir -p ~/Downloads/Temp


#Wget to grab the deb to install via distrobox later.
wget -P ~/Downloads/Software https://cdn.insynchq.com/builds/linux/insync_3.8.4.50481-jammy_amd64.deb


#Nvidia Drivers
#First, add Nvidia Repo

zypper addrepo --refresh https://download.nvidia.com/opensuse/tumbleweed NVIDIA

#Then add packages - for my laptop which this is designed for, I use the g06
sudo transactional-update pkg install -y nvidia-video-G06 x11-video-nvidiaG06

#For more info on openSUSE nvidia visit https://en.opensuse.org/SDB:NVIDIA_drivers 

#Neofetch (for flexing on 'em w/ desktop screenshots), Xkill , nmap, inxi
sudo transactional-update pkg install -y neofetch xkill nmap inixi

#DESKTOP SPECIFIC TWEAKS

# PLASMA 
#sudo transactional-update pkg install pam_kwallet

# GNOME 
#Some of these, I got from Tio Jorge Castro - via Ublue.it https://github.com/castrojo/ublue/blob/main/01-desktop.sh 
#What a treasurer. 

#figure out how to install blur background, blur login, app indicators, caffeine 

##Turn to 12 hr format
gsettings set org.gnome.desktop.interface clock-format '12h'   

##Enable dark theme - have to figure out universally, bc this only changed my terminal. 
#maybe because i have to logout and log back in

gsettings set org.gnome.desktop.interface gtk-theme "Adwaita-dark"

#more minor tweaks
dconf write /org/gnome/shell/disable-extension-version-validation "true" #yolo
dconf write /org/gnome/shell/disable-user-extensions "false"
dconf write /org/gnome/desktop/wm/keybindings/activate-window-menu "['<alt>space']"

dconf write /org/gnome/desktop/wm/keybindings/switch-to-workspace-1 "['<alt>1']"
dconf write /org/gnome/desktop/wm/keybindings/switch-to-workspace-2 "['<alt>2']"
dconf write /org/gnome/desktop/wm/keybindings/switch-to-workspace-3 "['<alt>3']"
dconf write /org/gnome/desktop/wm/keybindings/switch-to-workspace-4 "['<alt>4']"

dconf write /org/gnome/nautilus/preferences/default-folder-viewer "'grid-view'"
#if on gnome (probe function?)
#sudo transactional-update pkg install NetworkManager-openconnect -y
#auto-key via distrobox

#figure out how to call a transactional-update shell via script - and install 
#nvidia drivers in case im on nvidia (maybe figure out a probe function first?)
#then tailscale 

#DISTROBOX

#ubuntu bc Canonical raised me ;) 
distrobox-create -Y -i ubuntu:22.04 -n ubuntu

#might have to play around w/ distrobx exec to pull insync deb since they don't 
#really have a clean opensuse way to install insync


#then install gdebi after entering ubuntu box, sudo apt install gdebi.
#then run gui gdebi with sudo gdebi-gtk
#then install insync - note might have to go down to distrobox 20.04 instead
#since im running into issues w/ exporting.  

#run it to login with insync start - after you are done logging in...
#then export service with the command:
#distrobox-export --service insync
#then update the insync systemd service file (found in etc/systemd/system 
#and change the one line in Exec to read "Exec=distrobox enter ubuntu -- insync start 
#note: the spacing of the -- before and after the dashes matter! 
#save and reload the service insync,
# consider auto-key, and tilix next in distrobox and export. 

#APPS

#I need to install the following flatpaks. Before I do, I need to enable flatpak repo from terminal. 

flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo -y


#Android Studio
#flatpak install flathub com.google.AndroidStudio -y

#AnyDesk
flatpak install flathub com.anydesk.Anydesk -y

#Audacity
flatpak install flathub org.audacityteam.Audacity -y

#Brave
flatpak install flathub com.brave.Browser -y

#Blackbox Terminal
#flatpak install flathub com.raggesilver.BlackBox -y

#Chrome
flatpak install flathub com.google.Chrome -y

#Cheese Webcam
flatpak install flathub org.gnome.Cheese -y

#Chromium
#flatpak install flathub org.chromium.Chromium -y

#Discord
#flatpak install flathub com.discordapp.Discord -y

#Disk Uage Analyzer 
flatpak install flathub org.gnome.baobab -y

#Ksnip
flatpak install flathub org.ksnip.ksnip -y

#Epiphany Web
#flatpak install flathub org.gnome.Epiphany -y

#Kdenlive
flatpak install flathub org.kde.kdenlive -y

#OnlyOffice
flatpak install flathub org.onlyoffice.desktopeditors -y

#MasterPDF Editor
flatpak install flathub net.codeindustry.MasterPDFEditor -y

#Mumble
flatpak install flathub info.mumble.Mumble -y

#OBS-Studio
flatpak install flathub com.obsproject.Studio -y

#OpenShot
#flatpak install flathub org.openshot.OpenShot -y

#Polari#
flatpak install flathub org.gnome.Polari -y

#Standard Notes

#Thunderbird Email
#flatpak install flathub org.mozilla.Thunderbird -y

#Tor
flatpak install flathub com.github.micahflee.torbrowser-launcher -y

#Telegram
flatpak install flathub org.telegram.desktop -y

#Transmission
flatpak install flathub com.transmissionbt.Transmission -y

#VLC
flatpak install flathub org.videolan.VLC -y

#vscode

#Wavebox
flatpak install flathub io.wavebox.Wavebox -y

#Xournal
flatpak install flathub com.github.xournalpp.xournalpp -y

#Alphabet Video Downloader ;P
flatpak install flathub me.aandrew.ytdownloader -y

#Zim Wiki
flatpak install flathub org.zim_wiki.Zim -y

#Zoom
flatpak install flathub us.zoom.Zoom -y





